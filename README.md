# model-gitlab-ultimate

Meltano .m5o models for data fetched using the GitLab API. 

Dedicated to entities only available for GitLab Ultimate or GitLab.com Gold accounts (e.g. Epics, Epic Issues, etc).

Check [model-gitlab](https://gitlab.com/meltano/model-gitlab) for the Model and Tables available to all GitLab accounts.
